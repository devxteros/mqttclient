package co.premize.mqttclient;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";
    AppService mService;
    Intent intentService;
    boolean mIsBound;
    Button btnInicio;
    Button btnFinaliza;
    EditText txtMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        intentService = new Intent(MainActivity.this, AppService.class);
        intentService.putExtra(Constants.BROKER_HOST, "192.168.2.179");
        intentService.putExtra(Constants.BROKER_PORT, "1883");
        intentService.putExtra(Constants.BROKER_USER, "s3rv3r4ppcli3nts");
        intentService.putExtra(Constants.BROKER_PASS, "53rv3r4ppcl1ent509875");
        startService(intentService);
        doBindService();

        txtMessage = (EditText) findViewById(R.id.txt_message);
        btnInicio = (Button) findViewById(R.id.btnIniciarServicio);
        btnFinaliza = (Button) findViewById(R.id.btnDetenerServicio);


        btnInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startService(intentService);
            }
        });

        btnFinaliza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopService(intentService);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        doUnbindService();
        Log.v(TAG,"DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD->onDestroy Activity");
    }

    public void onClickBtnSend(View v){
        Toast.makeText(this,txtMessage.getText(),Toast.LENGTH_LONG).show();
    }

    public void enviarNotificacion(View v){
        mService.setNotification("Mensaje de prueba desde activity");
    }

    public void leerDatos(View v){
        try {
            mService.leerDatos();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = ((AppService.LocalBinder) service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
        }
    };

    private void doBindService(){
        bindService(intentService, serviceConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    private void doUnbindService(){
        if (mIsBound) {
            unbindService(serviceConnection);
            mIsBound = false;
        }
    }
}
