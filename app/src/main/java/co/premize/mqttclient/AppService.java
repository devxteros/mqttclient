package co.premize.mqttclient;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;


public class AppService extends Service {
    private static final String TAG = "AppService";
    private static final String ACTION_POWER_CONNECTED = "android.intent.action.ACTION_POWER_CONNECTED";
    private static final String ACTION_POWER_DISCONNECTED = "android.intent.action.ACTION_POWER_DISCONNECTED";
    private static final String ACTION_BATTERY_LOW = "android.intent.action.BATTERY_LOW";
    private final IBinder binder = new LocalBinder();
    private final BroadcastReceiver PowerStatusReceiver = new PowerStatusReceiver();
    private MQTTHelper mqttHelper;
    private MediaPlayer player;

    public AppService() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        player = MediaPlayer.create(this, R.raw.intro);
        player.setLooping(true);
        player.setVolume(60, 60);

        //Broadcast Conexión
        IntentFilter powerConnectedFilter = new IntentFilter(ACTION_POWER_CONNECTED);
        registerReceiver(PowerStatusReceiver, powerConnectedFilter);
        //Broadcast Desconexión
        IntentFilter powerDisconnectedFilter = new IntentFilter(ACTION_POWER_DISCONNECTED);
        registerReceiver(PowerStatusReceiver, powerDisconnectedFilter);
        //Broadcast bateria nivel muy bajo
        IntentFilter powerBatteryLowFilter = new IntentFilter(ACTION_BATTERY_LOW);
        registerReceiver(PowerStatusReceiver, powerBatteryLowFilter);
        startMqtt();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String brokerHost, brokerPort, brokerUser, brokerPass;
        Log.v(TAG,"ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ->onStartCommand()");
        try {
            if (intent != null && intent.getExtras() != null ) {
                brokerHost = intent.getStringExtra(Constants.BROKER_HOST)+"";
                brokerPort = intent.getStringExtra(Constants.BROKER_PORT)+"";
                brokerUser = intent.getStringExtra(Constants.BROKER_USER)+"";
                brokerPass = intent.getStringExtra(Constants.BROKER_PASS)+"";
                Utils.saveSharePreferences(Constants.BROKER_HOST_ALIAS, brokerHost, this, true);
                Utils.saveSharePreferences(Constants.BROKER_PORT_ALIAS, brokerPort, this, true);
                Utils.saveSharePreferences(Constants.BROKER_USER_ALIAS, brokerUser, this, true);
                Utils.saveSharePreferences(Constants.BROKER_PASS_ALIAS, brokerPass, this, true);
            }

            player.start();

        }catch (Exception e){
            Log.v(TAG, e.getMessage());
        }
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        player.start();
        return binder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mqttHelper.mqttAndroidClient.disconnect();
        } catch (MqttException e) {
            Log.e(TAG,e.getMessage());
        }
        mqttHelper.mqttAndroidClient.unregisterResources();
        unregisterReceiver(PowerStatusReceiver);

        if (player.isPlaying()) {
            player.stop();
        }
        player.release();
        player = null;
    }




    /**** Método de acceso ****/
    public class LocalBinder extends Binder {
        public AppService getService() {
            return AppService.this;
        }
    }

    public void setNotification(String contentText){
        Utils.sendNotification(contentText,this);
    }

    public void leerDatos(){
        System.out.println("DECODE: " + Utils.getSharedPreferences("A1", this, true));
        System.out.println("DECODE: " + Utils.getSharedPreferences("A2", this, true));
        System.out.println("DECODE: " + Utils.getSharedPreferences("A3", this, true));
        System.out.println("DECODE: " + Utils.getSharedPreferences("A4", this, true));
    }

    private void startMqtt() {
        mqttHelper = new MQTTHelper(this);
        mqttHelper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {
                Log.v(TAG,"ZZZZZZZZZZZZZZZZZ->connectComplete()="+b+" "+s);
            }

            @Override
            public void connectionLost(Throwable throwable) {
                Log.v(TAG,"ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ->connectionLost()");
                //Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                //v.vibrate(800);
            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                Log.w(TAG, mqttMessage.toString());
                //dataReceived.setText(mqttMessage.toString());
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(1000);
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

            }
        });
    }
}
