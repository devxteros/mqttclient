package co.premize.mqttclient;

/**
 * Created by Juan Manuel Lora on 06/12/2017.
 * co.premize.mqttclient
 */

public class Constants {
    public static final String BROKER_HOST = "BROKER_HOST";
    public static final String BROKER_PORT = "BROKER_PORT";
    public static final String BROKER_USER = "BROKER_USER";
    public static final String BROKER_PASS = "BROKER_PASS";

    //Usamos Alias para las llaves en Shared Preferences (datos sensibles)
    public static final String BROKER_HOST_ALIAS = "A1";
    public static final String BROKER_PORT_ALIAS = "A2";
    public static final String BROKER_USER_ALIAS = "A3";
    public static final String BROKER_PASS_ALIAS = "A4";
}
