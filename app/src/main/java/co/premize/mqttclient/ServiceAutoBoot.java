package co.premize.mqttclient;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ServiceAutoBoot extends BroadcastReceiver {

    public static final String TAG = ServiceAutoBoot.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            Intent service = new Intent(context, AppService.class);
            context.startService(service);
        }
        Log.v(TAG, "XXXXXXXXXXXXXXXXXXXXXXXX->"+intent.getAction());
    }
}
