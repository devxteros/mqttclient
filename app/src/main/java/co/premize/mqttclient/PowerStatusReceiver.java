package co.premize.mqttclient;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.widget.Toast;

import static android.content.Intent.ACTION_BATTERY_LOW;
import static android.content.Intent.ACTION_POWER_CONNECTED;
import static android.content.Intent.ACTION_POWER_DISCONNECTED;

/**
 * Created by PC on 06/12/2017.
 * co.premize.mqttclient
 */

public class PowerStatusReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL;
        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        float batteryPct = level / (float) scale;


        if (intent.getAction() == ACTION_POWER_CONNECTED) {

            if (isCharging == true) {

                Toast.makeText(context, "FULL", Toast.LENGTH_SHORT).show();

            }
        }

        if (intent.getAction() == ACTION_POWER_DISCONNECTED) {
            if (batteryPct >= 0.50) {
                Toast.makeText(context, "FULL", Toast.LENGTH_SHORT).show();
            } else if (batteryPct < 0.50) {
                Toast.makeText(context, "AHORRO", Toast.LENGTH_SHORT).show();
            }
        }

        if (intent.getAction() == ACTION_BATTERY_LOW) {
            Toast.makeText(context, "TERMINA SERVICIO", Toast.LENGTH_SHORT).show();
            Intent service = new Intent(context, AppService.class);
            context.stopService(service);
        }
    }
}
