package co.premize.mqttclient;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.NotificationCompat;
import android.util.Base64;
import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;


/**
 * Created by Juan Manuel Lora on 05/12/2017.
 * co.premize.mqttclient
 */

public class Utils {

    private static final String TAG = "Utils";


    /**
     * Envia una notificacion local a la barra de estado
     * @param contentText
     * @param context
     */
    public static void sendNotification(String contentText, Context context){

        Intent i = new Intent(context, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pi = PendingIntent.getActivity(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(context)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setColor(ContextCompat.getColor(context, R.color.colorAccent))
                .setTicker(context.getString(R.string.app_name))
                .setPriority(Notification.PRIORITY_MAX)
                .setContentTitle(context.getString(R.string.lbl_notification))
                .setContentText(contentText)
                .setContentInfo(context.getString(R.string.msg_title_noti_small_info))
                .setGroupSummary(true)
                .setAutoCancel(true)
                .setContentIntent(pi);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, builder.build());
    }


    /**
     * Guarda una cadena en las preferencias compartidas y valida si el dato debe ser cifrado
     * @param key
     * @param value
     * @param context
     * @param encode
     */
    public static void saveSharePreferences(String key, String value, Context context, boolean encode){
        try {
            String put = (encode == true)? Utils.encodeAES(value,context.getPackageName()) : value;
            SharedPreferences sharedPreferences = context.getSharedPreferences("shared_pref", context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(key, put);
            editor.commit();

        }catch (Exception e){
            Log.e(TAG,e.getMessage());
        }
    }

    /**
     * Lee en las preferencias compartidas el valor de la Key y valida si la decifra
     * @param key
     * @param context
     * @param decode
     * @return
     */
    public static String getSharedPreferences (String key, Context context, boolean decode){
        SharedPreferences sharedPreferences = context.getSharedPreferences("shared_pref", context.MODE_PRIVATE);
        String value = sharedPreferences.getString(key, "");
        try {
            value = (decode == true) ? decodeAES(value, context.getPackageName()): value;
        } catch (Exception e) {
            Log.v(TAG,e.getMessage());
        }
        return value;
    }


    /**
     * Cifra una cadena usando algoritmo AES
     * @param str
     * @param keyPass
     * @return
     * @throws Exception
     */
    public static String encodeAES(String str, String keyPass) throws Exception {
        final byte[] bytes = str.getBytes("UTF8");
        final Cipher aes = getCipher(Cipher.ENCRYPT_MODE, keyPass);
        byte[] encodeValue = aes.doFinal(bytes);
        encodeValue = Base64.encode(encodeValue, Base64.DEFAULT);
        return new String(encodeValue);
    }

    /**
     * Descifra una cadena que fue encriptada mediante la funcion encodeAES()
     * @param str
     * @param keyPass
     * @return
     * @throws Exception
     */
    public static String decodeAES(String str, String keyPass) throws Exception {
        final byte[] decodeValue = Base64.decode(str, Base64.DEFAULT);
        final Cipher aes = getCipher(Cipher.DECRYPT_MODE, keyPass);
        final byte[] bytes = aes.doFinal(decodeValue);
        final String decodeStr = new String(bytes, "UTF8");
        return decodeStr;
    }


    private static Cipher getCipher(int cryptMode, String keyPass) throws Exception {
        final MessageDigest digest = MessageDigest.getInstance("SHA");
        digest.update(keyPass.getBytes("UTF8"));
        final SecretKeySpec key = new SecretKeySpec(digest.digest(), 0, 16, "AES");
        final Cipher aes = Cipher.getInstance("AES/ECB/PKCS5Padding");
        aes.init(cryptMode, key);
        return aes;
    }

    /**
     * Encrypta una cadena usando el algoritmo SHA-256
     * @param input
     * @return String
     * @throws NoSuchAlgorithmException
     */
    public static String sha256(String input) throws Exception {
        MessageDigest mDigest = MessageDigest.getInstance("SHA256");
        byte[] result = mDigest.digest(input.getBytes("UTF8"));
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }


}
